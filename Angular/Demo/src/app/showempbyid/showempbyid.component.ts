import { Component } from '@angular/core';

@Component({
  selector: 'app-showempbyid',
  templateUrl: './showempbyid.component.html',
  styleUrl: './showempbyid.component.css'
})
export class ShowempbyidComponent {

  employees: any; 
  emp: any;
  msg: string;
  emailId: any;

  constructor() {
    this.msg = "";
    this.emailId = localStorage.getItem('emailId');

    this.employees = [
      {empId:101, empName:'Sravan', salary:1212.12, gender:'Male',   country:'IND', doj:'06-13-2018'},
      {empId:102, empName:'Sai',  salary:2323.23, gender:'Male',   country:'CHI', doj:'07-14-2017'},
      {empId:103, empName:'Sruthi', salary:3434.34, gender:'Female', country:'USA', doj:'08-15-2016'},
      {empId:104, empName:'Kubra',  salary:4545.45, gender:'Female', country:'FRA', doj:'09-16-2015'},
      {empId:105, empName:'Samrat',  salary:5656.56, gender:'Male',   country:'NEP', doj:'10-17-2014'}
    ];
  }

  getEmployee(employee: any) {

    this.emp = null;
   
    this.employees.forEach((element: any) => {
      if (element.empId == employee.empId) {
        this.emp = element;
      }
    });

    this.msg = "Employee Record Not Found!!!";

  }


}
